FROM fedora:34

# sys utils
RUN dnf install -y \
    file-5.39 \
    findutils-4.8.0 \
    git-2.31.1 \
    && dnf clean all

# compilers, interpreters and build tools
RUN dnf install -y \
    gcc-11.2.1 \
    gcc-c++-11.2.1 \
    clang-12.0.1 \
    clang-tools-extra-12.0.1 \
    clang-analyzer-12.0.1 \
    make-4.3 \
    cmake-3.20.5 \
    ninja-build-1.10.2 \
    arm-none-eabi-gcc-cs-10.2.0 \
    arm-none-eabi-gcc-cs-c++-10.2.0 \
    arm-none-eabi-newlib-4.1.0 \
    arm-none-eabi-binutils-cs-2.35 \
    python3-pip-21.0.1 \
    rubygems-3.2.22 \
    && dnf clean all

# checkers
RUN dnf install -y \
    ShellCheck-0.7.2 \
    && dnf clean all

# cmake-format
RUN pip install --no-cache-dir cmake-format==0.6.13

# markdown-linter
RUN gem install mdl:0.11.0

# zeromq
RUN dnf install -y \
    czmq-devel-4.2.1 \
    && dnf clean all

# protobuf
RUN dnf install -y \
    protobuf-compiler-3.14.0 \
    python3-protobuf-3.14.0 \
    && dnf clean all
